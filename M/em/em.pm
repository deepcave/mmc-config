package mmc::em;
use mmc::var;

sub put {
    my $name = shift;
    P::msg ("����� ������� $name") if (mmc::mode::debug("events"));
    return if (!$mmc::em::data{$name});
    my $cnt = 0;
    for ($i=0;$i<=5;$i++){
        for $code (@{$mmc::em::data{$name}{"$i"}}) {
            P::msg sprintf("call ($i) code  for %s:%u",$name,$cnt++) if (mmc::mode::get("debug2"));
            &$code (@_);
        }
    }
}

sub reg {
    my ($name,$code,$prio) = @_;
    $prio = 1 if (!$prio);
    if ($prio<0 || $prio>3) {
        P::warn("��������� ������� ����� ���� ������ 0-3");
        return;
    }
    if (!$mmc::em::data{$name}) {
        for ($i=0;$i<=5;$i++) {
            $mmc::em::data{$name}{"$i"} = ();
        }
        P::msg ("����������� ������ ������� $name") if (mmc::mode::debug);    
    }
    P::msg ("����������� ����������� ($prio) ��� $name") if (mmc::mode::debug);    
    push @{$mmc::em::data{$name}{"$prio"}},$code;

}

sub fake2 {
    P::msg "fake2 called";
}

sub fake {
    P::msg "fake called";
}

BEGIN {
    P::msg "registr fake";
    mmc::em::reg('mob_die',\&fake);
    mmc::em::reg('mob_die',\&fake2);
}
1;