package M::log;

sub info {
    $time = M::strDateTime();
    $name =   shift(@_);
    $format = shift(@_);
    $line = sprintf("\003L[\003P%s\003L][% -4s\003L]\003H:\003H%s",$time,$name,$format);
    $line = sprintf($line,@_);
    P::wecho 1, $line;
}

sub misk {
    $time = M::strDateTime();
    $name =   shift(@_);
    $format = shift(@_);
    $line = sprintf("\003L[\003P%s\003L][% -4s\003L]\003H:\003H%s",$time,$name,$format);
    $line = sprintf($line,@_);
    P::wecho 3, $line;
}
1;