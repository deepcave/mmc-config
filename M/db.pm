package M::db;
use Encode;
use DBI;

sub new {
    my ($class,$file) = @_;
    my $self = {};
    $self->{file}=$file;
    $self->{stms}=();
	bless $self, $class;
    return $self;
}

sub lastInsertId {
    my ($self) = @_;
    return $self->getDb()->sqlite_last_insert_rowid();
}

sub getDb {
    my ($self) = @_;
    if (!$self->{file}) {
        return;
    }
    if (!$self->{db}) {
        $self->{db} = DBI->connect(sprintf("DBI:SQLite:database=%s",$self->{file}));
	}
    return $self->{db};
}

sub last {
    my ($self) = @_;
    return $self->lastInsertId();
}

sub exe () {
    my $self = shift;
    my $sql = shift;
    if (!$self->{stms}{$sql}) {
        $self->{stms}{$sql} = $self->getDb()->prepare($sql);
    }
    if (@_) {
        return $self->{stms}{$sql}->execute(toUTF(\@_));
    } else {
        return $self->{stms}{$sql}->execute();
    }
}

sub fetch {
    my $self = shift;
    my $sql = shift;
    return $self->{stms}{$sql}->fetch();
}

sub toUTF {
    my $utf = $_[0];
    my $result = ();
    while (my ($n,$row) = each(@$utf)) {
        if (ref $row) {
            push(@$result,fromUTF($row));
        } else {
            my $line = $row;
            Encode::from_to($line,'windows-1251','utf8');
            push(@$result,$line);
        }
    }
    return @$result;
}

sub fromUTF {
    my $utf = $_[0];
    my $result = ();
    while (my ($n,$row) = each(@$utf)) {
        if (ref $row) {
            push(@$result,fromUTF($row));
        } else {
            my $line = $row;
            Encode::from_to($line,'utf8','windows-1251');
            push(@$result,$line);
        }
    }
    return $result;
}

sub fetchAll {
    my $self = shift;
    my $sql = shift;
    return fromUTF ($self->{stms}{$sql}->fetchall_arrayref());
}

sub exec () {
    my $self = shift;
    my $sql = shift;
    if (!$self->{stms}{$sql}) {
        $self->{stms}{$sql} = $self->getDb()->prepare($sql);
    }
    if (@_) {

        $self->{stms}{$sql}->execute(toUTF(\@_));
    } else {
        $self->{stms}{$sql}->execute();
    }
    return fromUTF ($self->{stms}{$sql}->fetchall_arrayref());
}

1;