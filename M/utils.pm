package M;
use M::log;

use Digest::MD5 qw(md5 md5_hex md5_base64);

 
sub hash {
	return md5_hex( shift );
}

sub log {
	M::log::info(@_);
}

sub info {
	my $l = shift;
	P::echo sprintf "\003C#\003P%s",$l
}

sub dbg {
	my $l = shift;
	open(F,'>>./dbg.log');
	print(F strTime()."|$l|@_\n");
	close(F);
}
sub numToStr {
    my $d = shift;
	my $r = 0;
	
    if ($d>10000000) {
        $r = sprintf('%.1f�',$d/1000000);
    } elsif ($d>100000) {
		$r = sprintf('%.1fk',$d/1000);
	} else {
		$r = $d.'';
	}
	return $r;
}

sub div {
    my ($a,$b) = @_;
    my $c = $a%$b;
    return (($a-$c)/$b);
}

sub shortMudName {
    my $str = shift;
	my $del = shift;
	$del = '_' if (!$del);
    my $result = '';
    for my $pword (split(/\s+/,$str)) {
        if ($pword =~ /.{4,50}/) {
            $pword =~ s/(.{3}).*/$1/g;
            $result .= $pword.$del;
        } elsif ($pword =~ /.{1,10}/) {
			$pword =~ s/(.{2}).*/$1/g;
		}
	}
   	$result =~ s/$del$//g;
	$result =~ s/\.$//g if (length($result)>4);
    return $result;
}

sub lc {
	my ($line) = @_;
	$line =~ tr/(�-�)(A-Z)/(�-�)(a-z)/;
	return $line;
}

sub norm {
	my ($line) = @_;
	return M::fuc(M::lc($line));
}

sub fuc {
	my ($line) = @_;
	$line =~ s/^(.)/&M::uc($1)/e;
	return $line;
}

sub uc {
	my ($line) = @_;
	$line =~ tr/(�-�)(a-z)/(�-�)(A-Z)/;
	return $line;
}

sub strTime () {
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	my $strTime = sprintf('%02u:%02u:%02u',$hour,$min,$sec);
	return $strTime;
}

sub strDate () {
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	my $strTime = sprintf('%04u-%02u-%02u',$year+1900,$mon+1,$mday);
	return $strTime;
}

sub strDateTime () {
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	my $strTime = sprintf('%04u-%02u-%02uT%02u:%02u:%02u',$year+1900,$mon+1,$mday,$hour,$min,$sec);
	return $strTime;
}

sub strFileTime () {
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	my $strTime = sprintf('%04u.%02u.%02u.%02u.%02u.%02u',
	1900+$year,$mon,$mday,
	$hour,$min,$sec);
	return $strTime;
}

sub debug {
	my ($msg,$lvl) = @_;
	P::wecho 8, sprintf("[%s] : %s",strTime,$msg); #if ($U::debug);
}

sub get_color($$) {
	my ($line,$n) = @_;
	return if (!substr($_[0], 2*$_[1]+1, 1));
	return chr(ord(substr($_[0], 2*$_[1]+1, 1))+ord('A'));
}

1;