package wld::moves;

sub __saveToMem {
    my ($src,$dst,$direct) = @_;
    $data{$src} = () if (!$data{$src});
    return $data{$src}{$dst} if ($data{$src}{$dst});
    $data{$src}{$dst} = $direct if (!$data{$src}{$dst});
    wld::rooms::link($src,$dst,$direct);

    my $srcZone = wld::zones::getByRoomNum($src);
    my $dstZone = wld::zones::getByRoomNum($dst);
    if (!$dstZone) {
        P::msg "miss zone $dst";
        return;
    }
    if (!$srcZone) {
        P::msg "miss zone $src";
        return;
    }
    if ($srcZone->{num}!=$dstZone->{num}) {
        if ($srcZone->{num}) {
            $srcZone->addExit($src,$dst);
        } else {
            P::echo "\003J������: ��� �������� ������, �� ���������� ���� ��� ������� $src";
        }
    }
}

sub get {
    my ($src,$dst) = @_;
    return $data{$src} if (!$dst);
    return $data{$src}{$dst}{$direct};
}


sub __saveToDB {
    my ($src,$dst,$direct) = @_;
    wld::db()->exe('INSERT INTO moves (srcRoomNum,dstRoomNum,direct) VALUES (?,?,?)',$src,$dst,$direct);
}


sub save {
    my ($src,$dst,$direct) = @_;
    return if (!$direct);
    $direct = wld::direct($direct);
    return $data{$src}{$dst} if ($data{$src}{$dst});
    __saveToMem($src,$dst,$direct);
    __saveToDB($src,$dst,$direct);
}

sub load {
    $res = wld::db()->exec('SELECT srcRoomNum,dstRoomNum,direct FROM moves');
    for my $row (@$res) {
        __saveToMem($row->[0],$row->[1],$row->[2]);
    }
}

BEGIN {
    %data = ();
}
1;