package wld::rooms;
use wld::obj::zone;
use wld::obj::room;

sub get {
    my ($num,$name) = @_;
    return $data{$num} if ($data{$num});
    return if (!$name);
    save($num,$name);
    return $data{$num};
}

sub add {
}

sub getByNum {
    my ($num) = @_;
    return $data{$num};
}
sub __saveToMem {
    my ($num,$name) = @_;
    $data{$num} = new wld::room($num,$name);
    $dataByZone{$wld::rooms::data{$num}->{zoneNum}}{$num} = $data{$num};
}

sub getNameByNum {
    my ($num) = (@_);
    return $wld::rooms::data{$num}->{name};
}

sub __saveToDB {
    my ($num,$name) = @_;
    my $room = $wld::rooms::data{$num};
    my $sql = 'INSERT OR IGNORE INTO rooms (roomNum,zoneNum,roomName) VALUES (?,?,?)';
    wld::db()->exe($sql,$room->{num},$room->{zoneNum},$room->{name});
    return $wld::rooms::data{$num};
}

sub save {
    my ($num,$name) = @_;
    return if ($wld::rooms::data{$num});
    __saveToMem($num,$name);
    __saveToDB($num,$name);
    return if ($wld::rooms::data{$num});
}

sub link {
    my ($src,$dst,$direct) = @_;
    $srcRoom = $data{$src};
    $dstRoom = $data{$dst};
    if (!$srcRoom) {
        P::msg "lost room $src";
        return;
    }
    $srcRoom->addExit($direct,$dstRoom);
}

sub load {
    $res = wld::db()->exec('SELECT roomNum,zoneNum,roomName FROM rooms ORDER BY roomNum');
    for my $row (@$res) {
        P::wecho 7,"loadRoom ".$row->[0];
        __saveToMem($row->[0],$row->[2]);
    }
}

BEGIN {
    %data = ();
    %dataByZone = ();
}
1;