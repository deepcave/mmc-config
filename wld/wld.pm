package wld;
use mmc::data::mud;
use wld::utils;
use wld::zones;
use wld::rooms;
use wld::moves;
use wld::exits;
use wld::map;

sub zones {return wld::zones::get(@_)}
sub rooms {return wld::rooms::get(@_)}
sub moves {return wld::moves::get(@_)}
sub exits {return wld::exits::get(@_)}

sub walk {return wld::walk::walk(@_)}
sub route {return wld::route::route(@_)}

sub wld::direct {
    my ($direct,$reverse) = @_;
    return wld::directs::shDirect($direct) if (!$reverse);
    return wld::directs::shReverse($direct);
}


sub db { return mmc::data::mud::db };

BEGIN {
    db();
    P::msg "load zones";
    wld::zones::load();
    P::msg "load rooms";
    wld::rooms::load();
    P::msg "load moves";
    wld::moves::load();
    P::msg "load exits";
    wld::exits::load();
}
1;