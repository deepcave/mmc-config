package wld::zone;

sub new {
    my $class = shift;
    my ($num,$name,$opts) = @_;
    my %neis = ();
    my $self = {};
    bless $self,$class;
    $self->{num}=$num;
    $self->{name}=$name;
    $self->{nais}=%neis;
    return $self;
}

sub num {
    $self = shift;
    return $self->{num};
}

sub name {
    $self = shift;
    return $self->{name};
}


sub link {
    my $self = shift;
    my ($src,$dst) = @_;
    return if (!$src);
    return if (!$dst);
    my $zdst = wld::zones::get($dst);
    $self->{neis}{$zdst->{num}}=$zdst;
}

sub addExit {
    my $self = shift;
    my ($src,$dst) = @_;
    return if (!$src);
    return if (!$dst);
    my $dstZone = wld::zones::getByRoomNum($dst);
    $self->link($self->{num},$dstZone->{num});
    $self->{exits}{$src}{$dst}=$dstZone;
}
1;