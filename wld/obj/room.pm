package wld::room;

sub new {
    my $class = shift;
    my ($num,$name,$opts) = @_;
    my $roomZoneNum = $num % 100;
    my $zoneNum = ($num - $roomZoneNum)/100;
    my $self = {};
    
    bless $self,$class;
    $self->{num}=$num;
    $self->{name}=$name;
    $self->{zoneNum}=$zoneNum;
    $self->{roomZoneNum}=$roomZoneNum;
    $self->{exits}=();
    return $self;
}

sub addExit {
    my $self = shift;
    my ($direct,$room) = @_;
    $self->{exits}{$direct}=$room;
}

1;