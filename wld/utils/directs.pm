package wld::directs;

%wld::directs::data = (
	    'e'=>'e','������'=>'e','������'=>'e','�'=>'e',
	    'w'=>'w','�����'=>'w','�����'=>'w','�'=>'w',
        'n'=>'n','�����'=>'n','�����'=>'n','�'=>'n',
	    's'=>'s','��'=>'s','��'=>'s','�'=>'s',
        'u'=>'u','�����'=>'u','�����'=>'u','��'=>'u',
	    'd'=>'d','����'=>'d','����'=>'d','��'=>'d',
		'v'=>'d','^'=>'u',
		'E'=>'e','W'=>'w','N'=>'n','S'=>'s',
		'�'=>'e','�'=>'w','�'=>'s','�'=>'n'
    );

%wld::directs::reverse = (
    	'e'=>'w',
	    'w'=>'e',
	    'n'=>'s',
	    's'=>'n',
	    'd'=>'u',
	    'u'=>'d'
);

%wld::directs::names = (
    	'e'=>'������',
	    'w'=>'�����',
	    'n'=>'�����',
	    's'=>'��',
	    'd'=>'����',
	    'u'=>'����'
);

%wld::directs::names_in = (
    	'e'=>'�������',
	    'w'=>'������',
	    'n'=>'������',
	    's'=>'���',
	    'd'=>'�����',
	    'u'=>'�����'
);

sub name {
	my ($direct,$num) = @_;
	$num = 1 if (!$num);
	return $wld::directs::names{$direct} if ($num==1);
	return $wld::directs::names_in{$direct} if ($num==2);
}

sub shDirect {
	my ($direct) = @_;
    if (!$wld::directs::data{$direct}) {
        CL::warn("����������� ����������� $direct");
        return;
    }
	return $wld::directs::data{$direct};
}

sub shReverse {
	my ($direct) = @_;
    my $sdirect = $wld::directs::data{$direct};
    return if (!$sdirect);
    if (!$wld::directs::reverse{$sdirect}) {
        CL::warn("�� ������ ������ ��� ����������� $sdirect");
        return;
    }
	return $wld::directs::reverse{$sdirect};
}
1;