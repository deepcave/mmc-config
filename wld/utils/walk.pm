package wld::walk;
use M::em::em;

sub ts {
    return if (!@queue);
    my $cmd = shift(@queue);
    return if (!$cmd);
    if ($cmd eq 'step') {
        wld::walk::step() if ((!$wait)&&(!$stop)&&(!$pause));
    } else {
        P::msg("TS(GO) : $cmd");
        P::sendl("$cmd");
    }
}

sub go {
     push(@queue,'step');
}

sub walk {
    if (!@_) {
        CL::warn "����� � ������� �������";
        return;
    }
    %wld::walk::route = ();    
    $stop = 0;
    $strwalk = '';
    while (my $room =shift(@_)) {
        my $next = $_[0];
        next if (!$next);
        my $cr = wld::rooms($room);
        my $nr = wld::rooms($next);
        my $f1 = 0;
        for my $direct (keys %{$cr->{exits}}) {
            next if ($f1);
            if ($cr->{exits}{$direct}==$nr) {
                $wld::walk::route{$room}{go}=$direct;
                $f1 = 1;
                $strwalk .= $direct;
            }
        }
    }
    return $strwalk
}

sub step {
    return if ($stop);
    my $roomNum = var::get("map","room");
    my %re;
    %re = %{$wld::walk::route{$roomNum}} if ($wld::walk::route{$roomNum});
    if ($re{go}) {
        P::echo (sprintf("\003D###��������� \003K����\003P: \003O%s",$re{go}));
        P::sendl($re{go});
    } else {
        P::echo "\003D###��������� \003J����� ������";
        $stop = 1;
        %route = ();
        return;
    }
}

sub reset {
    $stop = 1;
    %route = ();
}

BEGIN {
    wld::walk::reset;
    P::timeout {
        wld::walk::ts;
    } 100,0;

    mmc::em::reg("going",\&go,3);
}
1;