package wld::route;

sub init {
    %V = ();
    %G = ();   
}

sub route {
    my ($src,$dst)=@_;
    wld::route::init();
    wld::route::build($src);
    if ($V{$dst}) {
        my @route = path($src,$dst);
        return @route;
    } else {
        return;
    }
}

sub path {
    my ($src,$dst) = @_;
    my @route = ();
    my $stop = 0;
    my $room = wld::rooms::getByNum($dst);
    my $c = 0;
    unshift @route,$room->{num};
    
    while (!$stop) {
        $c++;
        my @exits = keys(%{$G{$room->{num}}});
        my $min = $V{$room->{num}};
        my $min_room = undef;
        for my $exit (@exits) {
            if ($G{$room->{num}}{$exit}<$min) {
                $min = $G{$room->{num}}{$exit};
                $min_room = $exit;
            }
        }
        $room = wld::rooms::getByNum($min_room);
        unshift @route,$room->{num};
        return @route if ($room->{num}==$src);
        return if ($c>250);
    }
}

sub build {
    my ($rn)=@_;
    my $room = wld::rooms::getByNum($rn);
    return if (!$room);
    $V{$room->{num}}=0 if (!$V{$room->{num}});
    my $mi = $V{$room->{num}};
    return if ($mi>250);
    #P::msg "room ".$room->{num};
    my @exits = keys(%{$room->{exits}});
    for my $exit (@exits) {
        my $eroom = $room->{exits}{$exit};
        #�������� ��� ������� � ����� !��������
        next if ($eroom->{zoneNum} == 39 || $eroom->{zoneNum} == 38);
        if ((!$V{$eroom->{num}}) || $V{$eroom->{num}}>($mi+1)) {
            $G{$eroom->{num}}{$room->{num}}=$mi;
            $V{$eroom->{num}} = $mi+1;
            build($eroom->{num});
        }
    }
}

BEGIN {
    %G = ();
    %V = ();
}

1;