package wld::zones;
use wld::obj::room;
use wld::obj::zone;

sub db {
    return wld::db();
}

sub get {
    my ($num) = @_;
    return wld::zones::getByNum($num);
}

sub add {
    my ($num,$name) = @_;
    save($num,$name);
}

sub __get {
    my ($num) = @_;
}

sub __saveToMem {
    my ($num,$name) = @_;
    $wld::zones::data{$num} = new wld::zone($num,$name);
    return $wld::zones::data{$num};
}

sub getNameByNum {
    my ($num) = (@_);
    return $wld::zones::data{$num}->{name};
}

sub getByRoomNum {
    my ($roomNum) = (@_);
    return if (!wld::rooms::get($roomNum));
    my $num = wld::rooms::get($roomNum)->{zoneNum};
    return $wld::zones::data{$num};
}

sub getByNum {
    my ($num) = (@_);
    return $wld::zones::data{$num};
}

sub getZoneNumByRoomNum {
    my ($roomNum) = (@_);
    my $roomZoneNum = $roomNum % 100;
    my $zoneNum = ($roomNum -$roomZoneNum)/100;
    return $zoneNum;
}

sub __getAll {
    return %data;
}
sub __saveToDB {
    my ($num) = @_;
    wld::db()->exe('INSERT INTO zones (zoneNum,zoneName) VALUES (?,?)',$num,$data{$num}->{name});
    return $data{$num};
}

sub link {

}

sub save {
    my ($num,$name) = @_;
    return if ($wld::zones::data{$num});
    wld::zones::__saveToMem($num,$name);
    wld::zones::__saveToDB($num,$name);
    return if ($wld::zones::data{$num});
}

# �������� ����� �� ��
sub load {
    $res = wld::db()->exec('SELECT zoneNum,zoneName FROM zones');
    for my $row (@$res) {
        wld::zones::__saveToMem($row->[0],$row->[1]);
    }
}

BEGIN {
    %data = ();
    %dataByZone = ();
}
1;