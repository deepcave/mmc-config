package wld::exits;

sub on_get_exit {
    my ($src,$dst,$direct) = @_;
    if (!$wld::exits::data{$src}{$direct}) {
        P::msg "save exit $direct from $from to $to" if (mmc::mode::debug);
        __saveToMem($src,$dst,$direct);
        __saveToDB($src,$dst,$direct);
    }
    return 1;
}

sub get {
    my ($src,$dst) = @_;
    return $data{$src} if (!$dst);
    return $data{$src}{$dst}{$direct};
}

sub __saveToMem {
    my ($src,$dst,$direct) = @_;
    $wld::exits::data{$src}{$direct}=$dst;
    return $wld::zones::data{$src};
}

sub __saveToDB {
    my ($src,$dst,$direct) = @_;
    P::msg("SAVE $src/$dst/$direct");
    wld::db()->exe('INSERT INTO exits (srcRoomNum,dstRoomNum,direct) VALUES (?,?,?)',$src,$dst,$direct);
    return $data{$src}{$dst};
}

sub load {
    $res = wld::db()->exec('SELECT srcRoomNum,dstRoomNum,direct FROM exits');
    for my $row (@$res) {
        __saveToMem($row->[0],$row->[1],$row->[2]);
    }
}



BEGIN {
    P::msg "�������� �������";
    %data = ();
    mmc::em::reg("get_exit",\&on_get_exit);
}
1;