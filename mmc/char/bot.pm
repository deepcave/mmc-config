package mmc::char::bot;
no warnings 'experimental::smartmatch';
use M::utils;
use mmc::char::bot::parser;

sub reset {
    @mmc::char::bot::masters = ();
    mmc::char::bot::save;
}

sub add {
    my ($name) = @_;
    $name = M::norm($name);
    if ($name ~~ @mmc::char::bot::masters) {
        CL::warn "$name ��� �������� � ������ ��������"
    } else {
        P::msg "������� ������� $name";
        push (@mmc::char::bot::masters,$name);
        P::msg "������� ������ ��������: @mmc::char::bot::masters";
        mmc::char::bot::save();
    }
}

sub del {
    my ($name) = @_;
    my $line = var::get('bot','masters');
    P::msg "������ ������� $name";
    $line =~ s/,$name$//ge;
    $line =~ s/$name,//ge;
    var::set('bot','masters',$line);
    load();
}

sub save {
    my $line = join(',',@mmc::char::bot::masters);
    var::set('bot','masters',$line);
}

sub load {
    @mmc::char::bot::masters = split(/,/,var::get('bot','masters'));
}

BEGIN {

    P::alias {
        my $cmd = shift;
        if ($cmd) {
            for $name (@_) {
                mmc::char::bot::add($name) if ($cmd eq '+');
                mmc::char::bot::del($name) if ($cmd eq '-');
            }
        }
        CL::i_msg sprintf "������� ���� : %s",join(" ",@mmc::char::bot::masters);
    } '.���';

    @masters = ();
    mmc::char::bot::load();
}
1;