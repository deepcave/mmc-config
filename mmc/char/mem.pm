package mmc::char::mem;
use mmc::char::parsers::mem;

sub info {
    my ($act,$spell,$count,$color) = @_;
    $color = $color?$color:"\003P";
    P::echo "\003P###\003K�����$color $act \003L\"\003O$spell\003L\" \003P($color$count\003P).";
}

sub get {

}

sub onCast {
    my ($spell) = @_;
    $data{$spell}-- if ($data{$spell});
    $data{$spell} = 0 if (!$data{$spell});
    info("��������",$spell,$data{$spell},"\003C");
}

sub onMem {
    my ($spell) = @_;
    $data{$spell}=0 if (!$data{$spell});
    $data{$spell}++;
    info("������",$spell,$data{$spell});
}

sub onAddMem {
    my ($spell) = @_;
    return $data{$spell}=0 if (!$data{$spell});
    info("������� � ���",$spell,$data{$spell},"\003I");
}
sub setSpellCount {
    my ($spell,$count) = @_;
    $data{$spell}=$count;
}


sub showMem {
    my $info = getMem();
    my $cn = '';
    my $c = 0;
    my $d = 0;
    my @s;
    for $spellName (keys(%data)) {
        $c++;
        if ($c % 4 == 0) {
            P::sendl("�� ������: ".join(',',@s));
            $cn.='=' while (shift(@s));
            $d=0;
        } else {
            $d++;
            push(@s,sprintf("%s:%u",$spellName,$data{$spellName}));
        }
    }
    P::sendl("�� ������: ".join(',',@s)) if ($d>0)   
    
}

sub getMem {
    my $spellName = '';
    my @fullInfo;
    for $spellName (keys(%data)) {
        push(@fullInfo,sprintf("%s:%u%s",$spellName,$data{$spellName},''));
    }
    return join(',',@fullInfo);
}

sub loadMem {
    open(FM,'data/mem.info');
    my $info = <FM>;
    close(FM);
    P::msg("�����: $info");
    my $line = '';
    for $line (split(',',$info)) {
        my ($spell,$count) = split(':',$line);  
        P::msg("$line/$spell/$count");
        my $n = $count - $data{$spell};
        if ($n>0) {
            P::sendl(sprintf("�� ��������� ��� %u %s",$n,$spell));
            my $i=0;
            for ($i=0;$i<$n;$i++) {
                P::sendl(sprintf('���� !%s!',$spell));
            }
        } 
    } 
}

sub saveMem {
    my $info = getMem();
    P::msg(sprintf("save mem:%s",$info));
    open(FM,'>data/mem.info');
    print FM $info;
    close(FM);
}

sub onEndMem {

}

BEGIN {
    %data = ();
    mmc::em::reg("mem",\&onMem);
    mmc::em::reg("cast",\&onCast);
    mmc::em::reg("add_mem",\&onAddMem);
    mmc::em::reg("end_mem",\&onEndMem);
    mmc::em::reg("mem_count",\&setSpellCount);
}
1;