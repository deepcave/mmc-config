package mmc::char::bot;
no warnings 'experimental::smartmatch';
$bot_help = "
������� - ��� ��� ���
����� - ������ �� ����� ������ � ����� ���
���� .����������. - ��������� ���������� ������� � ������ � ������ ���
����� .����������. - ��������� ���������� ������� � ������ ������ �����
��� ����� - ���������� ���� �������� ����� ����� �������
��� ����� - ���������� ���� ������� ��� ��� �����������
������ ��� - :) ������ ��� ������
������ .�����. - � ���� �������� ����� � ���� ����� ��������� ������ ������ �����
������ - �������� �������
���� .���. - ����� �� ��������� ���
";

sub logCmd {
    my ($master,$cmd) = @_;
    P::echo sprintf "\003P���:[\003Q%s\003P] : \003K%s",$master,$cmd;
    #TODO:�������� ������ ���������� ����� ����� (������� � 10 ������)
    P::wecho 11, sprintf "������>\003D[\003P%s\003D]\003D[\003H%s\003D]\003D[\003F%s\003D] : \003K%s",
        M::strTime(),$mmc::wld::room->{num},$master,$cmd;
}

sub do {
    my ($master,$cmd) = @_;
    mmc::char::bot::logCmd(@_);

    my @scmd = split('/\s+/',$cmd);
    if ($cmd =~/^�����(.*)$/) {#�����
        P::sendl sprintf "�� ����.���� %s",var::get("bags","����");
        P::sendl sprintf "�� ���.����";
        P::sendl sprintf "����� ����.����";
    } elsif ($cmd =~/^�������/) {
        for my $line (split(/\n/,$bot_help)) {
            if ($line =~/^\s*$/) {

            } else {
                P::sendl("���� $master $line");
            }
        }
        return;
    } elsif ($cmd =~/^���� (.*)$/) {#��������� ����� �� ������ ���
        var::set('spell','����',$1);
        P::sendl ('�� ������ ����� ������� '.$1.' � ������ ���');
        return;
    } elsif ($cmd =~/^����� (.*)$/) {#��������� ����� �� �����
        var::set('spell','���',$1);
        P::sendl ('�� ������ ����� ������� '.$1.' � ���');
        return;
    } elsif ($cmd =~/^��� �����/) {#������ ������ ��� �� ������������
        mmc::char::mem::loadMem();
    } elsif ($cmd =~/^��� �����/) {#�������� ��� ������ ��������� (�� ������� � ���������)
        mmc::char::mem::showMem();
    } elsif ($cmd =~/^��� ������(\d+)$/) {
        var::set('spell','������',$1);
        P::sendl ('�� ������ ����� �������� ���� ���������� ������ '.$1);
        return;
    } elsif ($cmd =~/^���� (\S+)/) {
        P::sendl('� !�������! '.$1);
        return;
    } elsif ($cmd =~/^���� (\S+)/) {
        P::sendl("���� $1");
        return;
    } elsif ($cmd =~/^����� (\S+)/) {
        P::sendl("���� ��� ���� $1");
        return;
    } elsif ($cmd =~/^������/) {
        P::sendl("�� ������");
        return;
    }

    
    act($cmd) if ($cmd ~~ @act);
    P::sendl $cmd;
}

sub parse {
    my ($name,$msg) = @_;
    return 0 if (!(var::get('mode','���') eq '��'));
    if (M::norm($name) ~~ @mmc::char::bot::masters) {
        #������ ������.
        if ($msg =~ /^!(.*)$/) {
            mmc::char::bot::do($name,$1);
            return 1;
        }
        
        if ($msg =~ /^(\S+)\s+(.*)$/) {
            if (var::get('char','name') eq M::norm($1)) {
                mmc::char::bot::do($name,$2);
                return 2;
            }
        }
    }
}

1;