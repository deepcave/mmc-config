package mmc::char;

use M::utils;
use mmc::var;


use mmc::char::actions;
use mmc::char::mem;
use mmc::skills::skin;
use mmc::char::cmd;
use mmc::char::flags;

sub login {
    P::msg sprintf "LOGIN %s",
        var::get('char','name');
    P::sendl sprintf "%s %s",
        var::get('char','name'),
        var::get('char','password');
}

sub setFight {
    my ($state) = @_;
    if ($state != $mmc::char::fight) {
        if ($state) {
            $mmc::fight::sx = 1;
            $mmc::char::fight = $state;
        } else {
            $mmc::fight::sx = 0;
            $mmc::char::fight = $state;          
            mmc::em::put("end_fight");
            M::info "fight to ".$state;
            #mmc::fight::mag::reset 8;
            #mmc::fight::autoassist::onStay();
        }
    }
}

sub setNLE {
    my ($nle) = @_;
    my $delta = 0;
    if ($mmc::char::nle) {
        $delta = $mmc::char::nle - $nle;
    }
    $mmc::char::nle = $nle;
    var::up('char','ExStr',M::numToStr($nle));
    return 0 if ($delta==0);
    my $str = sprintf "%s\003K����:%s",
        $delta>0?'+':'-',
        $delta>0?"\003L".$delta:"\003L".$delta;
    P::echo $str;
    return $delta;
}

sub setHp {
    my ($hp) = @_;
    var::up('char','Hp',$hp);
}
sub setMv ($) {
    var::up('char','Mv',$_[0]);
}

sub setStats {

}

sub setName ($) {
    var::set('char','name', $_[0]) if ($_[0]);
}

sub setPassword ($) {
    var::set('char','password', $_[0]) if ($_[0]);

}

BEGIN {
    $mmc::char::nle = 0;
    $mmc::char::name = '';
    $mmc::char::password = '';
}

1;