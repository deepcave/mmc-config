package mmc::char::parsers::mem;

P::trig {
    mmc::em::put("prepare_cast",$1);
}
    '^�� ������������� ��������� ���������� \'(.*)\' �� (.*)\.$',
    'f10::CHAR-MEM';


P::trig {
    mmc::em::put("mem",$1);
}
    '^�� ������� ���������� "(.*)"\.$',
    'fg10:CHAR-MEM';


P::trig {
    mmc::em::put("cast",$1);
}
    '^�� ���������� ���������� "(.*)"\.$',
    'fg10:CHAR-MEM';

P::trig {
    mmc::em::put("add_mem",$1);
}
    '^�� ������� ���������� "(.*)" � ���� ����\.$',
    'fg10:CHAR-MEM';

P::trig {
    my $line = $1;
    for my $str (split(/\|/,$line)) {
        if ($str=~/^\[\s*(\d+)\]\s+(.*)$/) {
            my $spellname = $2;
            my $n = $1+0;
            $spellname =~ s/\s*$//g;
            mmc::em::put("mem_count",$spellname,$n);
        }
    }
    
}
    '^(.*)$',
    '-f100:CHAR-MEM-PARSE-TABLE';

P::trig {
    P::disable('CHAR-MEM-PARSE-TABLE');
    P::msg "stop parse mem full";

}
    '^$',
    '-f10:CHAR-MEM-PARSE-TABLE';

P::trig {
    P::disable('CHAR-MEM-PARSE-TABLE');
    P::msg "stop parse mem";
}
    '^  �� ����������� ��������� ���������� :$',
    'f10::CHAR-MEM';

P::trig {
    %mmc::char::mem::data = ();
    P::enable('CHAR-MEM-PARSE-TABLE');
}
    '^  �� ������ ��������� ���������� :$',
    'f10::CHAR-MEM';


1;