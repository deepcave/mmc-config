package mmc::group::events;

sub onChangeRoom {
    mmc::msdp::send("SEND","GROUP");
}

mmc::em::reg("msdp_room",\&onChangeRoom);
1;