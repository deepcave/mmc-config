package mmc::group::cmd;
use mmc::group::group;

BEGIN {
    P::alias {
        P::echo "\003K�������: @_";
        P::sendl ("��� ��� @_");
    } '\\';

    P::alias {
        my $tank = $mmc::group::members[$#mmc::group::members];
        P::echo "\003G$tank\003K> @_";
        P::sendl ("�� $tank @_");
    } "-";

    # ����� ������ (�������� ���)
    P::alias {
        my $tank = $mmc::group::members[$#mmc::group::members];
        P::echo "\003G".var::get("mode","���")."\003K> @_";
        P::sendl ("�� $tank ".var::get("mode","���")." @_");
    } "=";

    # ������� � ������ (!)
    P::alias {
        P::echo "\003K���> @_";
        P::sendl ("�� !@_");
    } '_';

    # ��������� ����.
    P::alias {
        P::echo "\003B���\003K> @_";
        for my $iname (@mmc::group::members) {
            next if ($iname eq var::get('char','name'));
            P::sendl ("�� $iname @_");
        }
    } '+-';
}
1;