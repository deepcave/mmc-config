package mmc::group;

P::trig {
	my $room = wld::rooms(var::get("map","room"));
	my $zone = wld::zones(var::get("map","zone"));
	P::sendl sprintf "�� ������������� � %s, %s [%s]",$zone->{name},$room->{name},$room->{num};
}
	'^�� �������� ������ ��������, ������� ���������� � ����\.$',
	'f10:GROUP-PARESER-MISK';

P::trig {
	mmc::group::reset();
	mmc::msdp::send("SEND","GROUP");
} 
    '^���� ������ ������� ��:$',
    'f10:MGROUP';

#TODO::������� � ��������� �����
sub parseIncome {
	my ($name,$type,$from) = ($1,$2,$3);
		if (mmc::group::isMember($name)) {
		P::echo sprintf "\003K###����� %s c %s",
			$name,$from if (var::get("group","�����") eq "��");
	} else {
		P::echo sprintf "\003J###����� %s c %s",
			$name,$from
	}
}

sub parseOutcome {
my ($name,$type,$to) = ($1,$2,$3);
	if (mmc::group::isMember($name)) {
		P::echo sprintf "\003L###\003P[\003D�����\003P] %s%s \003G %s",
			mmc::group::isMember($name,1)?"\003K":"\003L",$name,$to if (var::get("group","������") eq "��");
		if (mmc::group::isMember($name)) {
			P::sendl("�������� $to") if (var::get("group","��������") eq "��");
		}
	} else {
		P::echo sprintf "\003L###\003P[\003D�����\003P] %s%s \003G %s",
			mmc::group::isMember($name,1)?"\003K":"\003L",$name,$to;
	}
}

P::trig {
	parseOutcome($1,$2,$3);
}
	'^([^:]+) (������|�����|����|���|�����[�]?)[���]? �� (�����|��|�����|������)\.$',
	'fg10::LOOK-OUTCOME';

P::trig {
	parseOutcome($1,$2,$3);
}
	'^([^:]+) (������|�����|����|���|�����[�]?)[���]? (�����|����)\.$',
	'fg10::LOOK-OUTCOME';

P::trig {
	parseIncome($1,$2,$3);

}
	'^([^:]+) (��������|�������|������|�����|�������[�]?)[���]? � (\S+)\.',
	'fg10::LOOK-INCOME';

P::trig {
	parseIncome($1,$2,$3);
}
	'^([^:]+) (��������|�������|������|�����|�������[�]?)[���]? �(���|����)�\.',
	'fg10::LOOK-INCOME';

1;