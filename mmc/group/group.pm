package mmc::group;
use mmc::group::parser;
use mmc::group::cmd;
use mmc::group::events;

use Data::Dump qw(dump);
no warnings 'experimental::smartmatch';


sub reset {
	%mmc::group::data = ();
	@mmc::group::charms = ();
	@mmc::group::members = ();
	$mmc::group::leader = "";
}

sub set {
	my ($name,$pos,$hp,$mv,$role,$here,$affects) = @_;
	if ($role eq "leader") {
		$mmc::group::leader = $name;
	}
	#hack
	$name =~ s/�������/�������/g;

	my $dHp = 0;
	my $dMv = 0;
	if ($mmc::group::data{$name}) {
		$dHp = $hp - $mmc::group::data{$name}{hp};
		$dMv = $hp - $mmc::group::data{$name}{hp};
	} else {
		$mmc::group::data{$name} = ();
		push @mmc::group::members,$name if ($role eq "pc" || $role eq "leader");
		push @mmc::group::charms,$name if ($role eq "npc");
	}
	if ($dHp>5) {
		P::msg "$name ��������";
	}
	if ($hp<=10) {
		P::msg "$name ����� ������� ���";
	}
	if ($mv<=10) {
		P::msg "�� ���� �������� � $name";
	}
	$mmc::group::data{$name}{hp}=$hp;
	$mmc::group::data{$name}{mv}=$mv;
	$mmc::group::data{$name}{pos}=$pos;
	$mmc::group::data{$name}{role}=$role;
	$mmc::group::data{$name}{here}=$here;
	$mmc::group::data{$name}{affects}=$affects;
	if ($mmc::group::data{$name}{here}==0) {
		if (var::get("group","���������") eq "��") {
			P::echo sprintf "\003G###\003J��������!!! \003D��������� $name";
			P::sendl "�� ������ ������ ������ � ������� $name"
				if (var::get("group","�������") eq "��");
		}
	}
	$mmc::group::tank =  $mmc::group::members[$#mmc::group::members];
}

sub hasMissed {
	while (my($name,$d) = each(%mmc::group::data)) {
		next ($name ~~ split(var::get('group','�����')));
		return 1 if ($d{here}==0);
	}
	return 0;
}
sub print {
	my $strG = '';
	my $name = '';

	for $name (@mmc::group::members) {
		$strG.='*' if ($mmc::group::leader eq $name);
		$strG.=sprintf "[%s]",$mmc::group::membersRoom{$name} if ($mmc::group::membersRoom{$name});
		$strG.=$name.' ';
	}

	P::echo("\003O|\003K������:\003G ".$strG);
	P::echo("\003O|\003K�������:\003G ".join(', ',@mmc::group::charms));
}

sub isMember {
	my ($name) = (@_);
	return 1 if ($name ~~ @mmc::group::members);
	return 2 if (isCharmis($name));
	return 0;	
}

sub isCharmis {
	my ($name) = (@_);
	return 1 if ($name ~~ @mmc::group::charms);
	return 0;	

}

BEGIN {
	$flag = 0;
	mmc::em::reg("setGroupMember",\&set);
}
1;