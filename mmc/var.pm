package var;
sub up  {mmc::var::up(@_)}
sub set {mmc::var::set(@_)}
sub del {mmc::var::del(@_)}
sub in  {mmc::var::in(@_)}

sub get {
    my ($mode,$var) = @_;
    my $val = "";

    if (!$var) {
        $var = $mode;
        $mode = 'mode';
    }
    $val = mmc::var::get($mode,$var);
    return $val;
}
package mmc::mode;

sub set ($) {
    P::msg "set MMC mode $_[0]";
    mmc::var::set("mmc",$_[0],1);
}

sub off ($) {
    P::msg "unset MMC mode $_[0]";
    mmc::var::set("mmc",$_[0],0);
}

sub get ($) {
    return mmc::var::get("mmc",$_[0]);
}

sub debug {
    return mmc::var::get("mmc","debug");
}

package mmc::var;
use mmc::data::var;

my %data = ();

sub db {
    return mmc::data::var::db; 
}

sub vkeys {
    my ($mode) = @_;
    my @result = ();
    @result = keys(%mmc::var::data) if (!$mode);
    @result = keys(%{$mmc::var::data{$mode}});
    return @result
}

sub dataLink {
    return %mmc::var::data;
}

sub del {
    my ($mod,$key,$val) = @_;
    delete (${$mmc::var::data{$mod}}{$key});
    mmc::var::__drop($mod,$key);
}

sub in {
    my ($mod,$key,$val) = @_;
    my $str = $mmc::var::data{$mod}{$key};
    return index($str,$val)+1;
}

sub set {
    my ($mod,$key,$val) = @_;
    P::msg sprintf("set var %s:%s to %s",$mod,$key,$val) if (mmc::mode::debug);
    $mmc::var::data{$mod}{$key}=$val;
    mmc::var::__save($mod,$key,$val);
}

sub up {
    my ($mod,$key,$val) = @_;
    $mmc::var::data{$mod}{$key}=$val;
    #__save($mod,$key,$val);
}


sub get {
    my ($mod,$key,$f) = @_;
    return $mmc::var::data{$mod}{$key};
}

sub __drop {
    my ($mod,$key,$val) = @_;
    mmc::var::db->exe('DELETE FROM vars WHERE mod=? AND name=?',$mod,$key);    
}

sub __save {
    my ($mod,$key,$val) = @_;
    mmc::var::db->exe('INSERT OR IGNORE INTO vars (mod,name,value) VALUES (?,?,?)',$mod,$key,$val);
    mmc::var::db->exe('UPDATE vars SET value=? WHERE mod=? AND name=?',$val,$mod,$key);
}

sub __load {
    my $res = mmc::var::db()->exec('SELECT mod,name,value FROM vars');
    for my $row (@$res) {
        $mmc::var::data{$row->[0]}{$row->[1]}=$row->[2];
    }
}

#just for debug
sub __table {
    for my $m (keys(%mmc::var::data)) {
        for my $v (keys(%{$mmc::var::data{$m}})) {
            P::msg "Key [$m][$v]=".var::get($m,$v);
        }
    }

}

sub ping {
    return 0;
}

BEGIN {
    P::msg "Загрузка таблицы переменных";
    %mmc::var::data =();
    db();
    __load();
}
1;