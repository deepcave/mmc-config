package mmc::skills::stab;
use mmc::var;

sub trip {
    my ($target) = @_;
    P::sendl ("�������� $target");
}

sub stab {
    my ($target) = @_;
    P::sendl "�������� $target";
}

sub flee {
    my ($direct) = @_;
    P::sendl ("������ $direct");
}

sub sneak {
    my ($direct) = @_;
    return 0 if (!$wld::directs::data{$direct});
    P::sendl('�����');
    P::sendl($direct);
}

sub hide {
    P::sendl('�����')
}

sub do {
    my ($target,$direct,$flags) = @_;
    my $mode = var::get('fight','����');
    my ($trip,$stab,$flee,$sneak) = (0,1,1,1);
    $flags.='';
    my $fdirect = wld::directs::shReverse($direct);

    if (!$fdirect) {
        $sneak = 0;
        $flee = 0;
    }
    if ($mode) {
        $trip = 1 if ($mode=~ /[��]/);
        $flee = 0 if ($mode=~ /[��]/);
        $stab = 0 if ($mode=~ /[��]/);
    }
    if ($flags) {
        $trip = 1 if ($flags=~ /[1��]/);
        $flee = 0 if ($flags=~ /[��]/);
        $stab = 0 if ($flags=~ /[��]/);
    }
    #TODO: �����������
    #my $stab = ($flags=~/[��]/||$mode=~/[��]/)?0:1;
    #my $sneak = 1;
    #my $hide = 1;
    
    P::echo "\003B���� \003K $target : ".
            ($direct?"\003F ($direct) ":'').
            "\003O".
            ($sneak?'�������� ':'').
            ($trip ?'������� ':'').
            ($stab ?'������� ':'').
            ($flee ?"����� \003C(".$fdirect.')':'');
    sneak ($direct) if ($sneak);
    hide if ($hide);
    trip($target) if ($trip);
    stab($target) if ($stab);
    flee (wld::directs::shReverse($direct)) if ($flee&&wld::directs::shReverse($direct));
}

P::alias {
    my ($target,$direct,$f) = @_;
    mmc::skills::stab::do($target,$direct,"$f");
} '�����';

1;