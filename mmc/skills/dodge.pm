package mmc::skills::dodge;
use mmc::var;

P::trig {
    $mmc::fight::dodge::s = 0;
} '^������, �� ����������� ���������� �� ��������� �����\!$','f10:SKILLS-DODGE';

P::trig {
    $mmc::fight::dodge::s-=2;
} '^�� � ��� ���������, ��� ������.$','f10:SKILLS-DODGE';

sub do {
    return if ($mmc::fight::dodge::s++<1);
    my $mode = var::get('mode','�����');
    P::sendl('�����') if ($mode =~ /�/);
}

1;