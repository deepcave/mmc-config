package mmc::skills::stun;

sub stun {
    my ($target) = @_;
    P::sendl ('����� '.$target);
}

sub auto {
    my ($target,$st) = @_;
    my $mode = var::get('mode','����');
    mmc::skills::stun() if ($mode =~ /�/);
}

P::trig {
    mmmc::skills::stun::auto($1,0);
} '^�� ���������� �������� (.*), �� �� ������\.$','f100:SKILLS-STUN';

P::trig {
    mmmc::skills::stun::auto($1,0);
} '^���� ������ ����� �������� (.*)\.$','f100:SKILLS-STUN';

P::trig {
    mmmc::skills::stun::auto($1,0);
} '^��� ��������� ���� ���� (.*) � ���\.$','f100:SKILLS-STUN';

1;