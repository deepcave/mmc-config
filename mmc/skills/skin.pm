package mmc::skills::skin;


P::trig {
    my $mode = var::get('skin','����');
    my $get  = var::get('skin','���������');
    my $bag  = var::get('bags','����');
    
    $mmc::skills::skin::m_count++;
    P::sendl('�� ����') if (!($get eq "��"));
    P::sendl('��� ���� '.$bag) if (!($bag eq "���"));
}
    '^�� ����� ���������� ���� .*',
    'f10:SKILLS-SKIN';

P::trig {
    my $mode = var::get('skin','�����');
    my $get  = var::get('skin','���������');
    my $bag  = var::get('bags','�����');
    $mmc::skills::skin::s_count++;
    P::sendl('�� ����') if (!($get eq "��"));
    P::sendl('��� ���� '.$bag) if (!($bag eq "���"));
}
    '^�� ����� ������� (.*)',
    'f10:SKILLS-SKIN';

sub skin {
    my $mode = var::get('skin','���������');
    my $get  = var::get('skin','���������');
    my $bag  = var::get('bags','�����');
    return if ($mmc::char::fight);
    P::sendl "������" if (var::get("mode","��������") eq "��" && $mmc::char::ass == 1);
    P::msg "###skin $mode fight".$mmc::char::fight;
    while (my $name = shift(@mmc::skills::skin::queue)) {
        my $mname = M::shortMudName($name,'.');
        $mname =~ s/\s+/./;
        P::echo "###\003K#�������\003� �������� ���� $name ($mname)";
        P::sendl sprintf "����� ����.%s",$mname
            if ($get eq "��");
        P::sendl sprintf "���������� ����.%s",$mname
            if ($mode eq "��");
    }
}

sub process {
    my $cname = shift;
    #�������� � ����� ���, ���� � ����� ����� � ������� ���� ���, �������
    return if (var::get("skin","���������") eq "���");
    push @mmc::skills::skin::queue,$cname; 
    if ($mmc::char::fight) {
        P::echo "###\003K#�������\003� �������� � ������� $cname";
    } else {
        mmc::skills::skin::skin();
    }
}

mmc::em::reg("new_corp",\&process,3);
mmc::em::reg("end_fight",\&skin,3);
1;