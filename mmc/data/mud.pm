package mmc::data::map;

sub db {return mmc::data::mud::db}

package mmc::data::mud;
use M::utils;
use M::db;


sub db {
    if (!$mmc::data::mud::_db) {
        $mmc::data::mud::_db = new M::db('data/db/map.s3db');
    }   
    return $mmc::data::mud::_db;
}

BEGIN {
    db();
}
1;