#���� ������ ��� ��������
package mmc::data::var;
use M::db;

sub db {
    return $mmc::data::var::_db if ($mmc::data::var::_db);
    $mmc::data::var::_db = new M::db("data/db/settings.s3db");
    return $mmc::data::var::_db;
}

BEGIN {
    db();
}
1;