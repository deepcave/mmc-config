package mmc::fight::round;


sub onRound {
    $fight::round::num++;
    var::up('fight','round',$fight::round::num);
}

sub stop {
    $fight::round::num = 0;
    var::up('fight','round',$fight::round::num);
}

1;