package mmc::fight;
use mmc::fight::assist;

no warnings 'experimental::smartmatch';

sub color($) {
	return "\003".$_[0];
}

sub colored {
	return color($_[1]).$_[0].color('H');
}

sub getGType ($) {
	my $str = '';
	if ($_[0] eq '��' || $_[0] eq '���' ) {
		$str = 'O';
	} elsif (mmc::group::isMember($_[0])) {
		$str = '#';
	} elsif (mmc::group::isCharmis($_[0])) {
		$str = 'I';
	} else {
		$str = '=';
	}
	return $str;
}

my %typeColorMap = (
		'O'=>"L",
		'#'=>"K",
		'I'=>"C",
		'='=>"M"
);

sub coloredGType($) {
	my $type = getGType($_[0]);
	return colored('#',$typeColorMap{$type});
}

sub coloredShields ($) {
	my $line = shift;
	my $str = '';
	$str .= ($line=~/J/)?colored('#','J'):'#';
	$str .= ($line=~/P/)?colored('#','P'):'#';
	$str .= ($line=~/O/)?colored('#','O'):'#';

	return $str;
}

sub coloredAssist {
	my $mode = var::get("mode","����");
	return '-' if ($mode eq '���');
	return colored('#','K') if (!$mmc::fight::sx);
	return colored('#','M');
}

sub coloredHitStr {
	my ($name,$val,$htype,$target) = @_;
	my $type = getGType($name);
	my $line = '';
	if ($name eq '��') {
		$mcolor = 'L';
	} elsif ($target eq '���') {
		$mcolor = 'J';
	} elsif ($type eq '#' || $type eq 'I') {
		$mcolor = 'K';
		if (getGType($target) eq '#' ) {
			$line .= colored('!!!','J');
		}
	} else {
		$mcolor = 'O';
	}
	$line .= colored('[','P');
	$line .= colored($name,($target eq '���')?'J':$typeColorMap{$type});
	$line .= colored(']','P');
	$line .= colored(
		sprintf(" %s %s [%s]",$val,$htype,$target),
		$mcolor);

	return $line;	

}

sub lineDamage {
	my ($name,$val,$type,$target,$shields) = @_;
	$shields = "" if (!$shields);
	# 
	#  # ����, ��� ���
	#  # ��� ����
	#  ### ���� ����
	#  # ��������� (�������, �����, �����) # * - (�����,����,���������)

	# ":## ### #";
	#����:
	## @ - �
	## * - ������
	## # - ���������
	## : - �� ���
	
	my $line = coloredGType($name);
	$line .= coloredGType($target);
	$line .= coloredAssist;
	$line .= coloredHitStr($name,$val,$type,$target);
	$line .= colored('[','I').coloredShields($shields).colored(']','I') if ($shields);
	
	
	#sprintf "%s%s%s%s%s",
		
	P::echo $line;
}

sub react {
	my ($name,$val,$type,$target,$shields) = @_;
	
	if ($name eq '��') {#���� ��� �

	} elsif (mmc::group::isMember($name)) {#���� ���� ���������
		if ($name eq '���') {#������� ����� ���!
			P::sendl("������ $name");
		} elsif (mmc::group::isMember($target)) {#����� �����
			P::sendl("�� ��������� ������� $name � $target");
			P::sendl("� !����!");
			P::sendl("��� ���� ���� $name");
			#��� �� ���� ����, ����� ������� � �������, ��� �� ���� ����� ������ - ����� ������� �������
		} else {
			mmc::fight::assist::do($name,$target,1);#���������
		}
	} elsif (mmc::group::isCharmis($name)) {#���� ���� ������
		mmc::fight::assist::do($name,$target,1);#���������
	} elsif ($name eq '���') {#��� ����
		#��� ����!
	} elsif (mmc::group::isMember($target)) {#���� ����������
		#��������� ������� (��������� ��������)
	} else {#���-�� ����-�� ����		
	}
}

sub onDamage {
	my ($name,$val,$type,$target,$sh) = @_;
	my $shLine = '';
	
	if ($name ~~ @mmc::group::members) { #��������� ����-�� �������
		mmc::fight::assist::do($name,$target,1);
		return 0;
	} elsif ($name ~~ @mmc::group::myCharms || $name ~~ @group::charms) {
		mmc::fight::assist::do($name,$target,2);
		return 0;
	} else {
		if ($name eq '��') { #� ����-�� ��..�
		} elsif ($target eq '���') { #��� �..���
		} else { #���-�� ����-�� �..�
            #������������ ��� ���� ���
		}
	}
}

#������� �� �����
mmc::em::reg("onHit",\&react);
mmc::em::reg("onMiss",\&react);


mmc::em::reg("onHit",\&lineDamage);
1;