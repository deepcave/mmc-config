package mmc::fight::paresers::hit;

my @strHitPrefVal = (
    '����������',
    '�����������',
    '�����',
    '�����',
    '�����������'
);

my @strHitVals2 = (
    "��������",		#  1..5
	"������",		# 6..11
	"������",		# 27..35
    "������",		# 56..96	
    "�������",	# 217..256
	"������",# 257..296
	"�����������",	 # 297..400
	"����������", # 400+
);

my @strHitVals = (
    '������',
    '������',
    '�������',
    '�����������',
    '����������',
    '������',    
    '��������',
    '������'
);
my @strHitTypes2 = (
    "��������", "���������","��������", "�������",  "�������",
    "�������","�������","�������","�������","������","�������",
    "�������","�����������","���������","��������","���������","������"
);
my @strHitTypes = (
	"������",
	"�������", 
	"��������",
	"�������", 
	"������",
	"�����", 	
	"��������", 
	"�������", 
	"��������", 
	"����������", 
	"������", 
	"������", 
	"�����", 
	"������", 
	"������", 
	"������", 
	"������"
);

sub parseDamage {
    my $line = shift;
    
    my $str1 = sprintf '(%s)',join '|',@strHitPrefVal;
    my $str2 = sprintf '(%s)',join '|',@strHitVals;
    my $str3 = sprintf '(%s)',join '|',@strHitTypes;
    
    my $val = '';
    my $type = '';
    my $name = '';
    my $target = '';
    $val = $1 if ($line =~ m/$str1/);
    $val .= ' '.$1 if ($line =~ m/$str2/);
    $type = $1 if ($line =~ m/$str3/);
    $val =~ s/^\s+//;
    $str1=sprintf '(.*)\s+%s\s*%s[���]?\s+(.*)\.',$val,$type;
    
    if ($line =~ m/$str1/) {
        $name = $1;
        $target = $2;
    } else {
        #M::logError();
        P::msg $line;
        P::msg 'AHTUNG '.$str1;
    }
    return $name,$val,$type,$target;
}

#����� ����������� ������ ����������� ������ ���

my $str =  sprintf '^(.*)\s+(?:(%s\s+))?(?:(%s)\s+)?(?:(%s)[���]?\s+)([^.]*)\.( \(.*\))?$',
    join ('|',@strHitPrefVal),
    join ('|',@strHitVals),
    join ('|',@strHitTypes);
P::msg $str;
#my $str = sprintf ('^(.*)\s+()(%s?)(%s)[���]?\s+([^.]*)\.( \(.*\))?$',join('|',@strHitVals),join('|',@strHitTypes));
#my $str2 = sprintf ('^(.*)\s+(%s)[���]?\s+([^.]*)\.( \(.*\))?$',join('|',@strHitTypes));

P::echo "Hit TRIG:".$str;
#������� ������ ��������� ��������� ���� ������

sub parseShield {
    my $line = shift;
    $line=~s/[^(]+\(.(.*)\).$/$1/g;
    my $i=0;
    my @res;
    my $c = '';
    while ($c = M::get_color($line,$i)) {
        push(@res,$c);
        $i++;
    }
    return (join('#',@res));
}

P::trig {
    my $line = $_;
    
    my $str1 = sprintf '(%s)',join '|',@strHitPrefVal;
    my $str2 = sprintf '(%s)',join '|',@strHitVals;
    my $str3 = sprintf '(%s)',join '|',@strHitTypes;
    my $val = '';
    my $type = '';
    my $name = '';
    my $target = '';

    $val = $1 if ($line =~ m/$str1/);
    $val .= ' '.$1 if ($line =~ m/$str2/);
    $type = $1 if ($line =~ m/$str3/);
    $val =~ s/^\s+//;
    $str1=sprintf '(.*)\s+%s\s*%s[���]?\s+(.*)\.',$val,$type;
    
    if ($line =~ m/$str1/) {
        $name = $1;
        $target = $2;
    } else {
        P::msg $line;
        P::msg 'AHTUNG '.$str1;
    }
    #($name,$val,$type,$target,$sh)
    
    my @V = ($name,$val,$type,$target);
    push(@V,parseShield($;)) if ($line =~ /\([^)]\)$/);
    
    for (@V) {
        s/\s+$//g;
        s/^\s+//g;
    }
    mmc::em::put("onHit",@V);
}   $str,
	'g3002:FIGHT-PARSERS-HIT';

P::trig {
    my @V = ($2,0,"��������",$1);
    mmc::em::put("onMiss",@V);
}
    '^(.*) ����� �������� ����� ([^.]+)\.$',
    'f3000:FIGHT-PARSER-HIT';

P::trig {
    my @V = ($1,0,"��������",$2);
    mmc::em::put("onMiss",@V);
}
    '^���� (.*) ������ ���� (.*)\.$',
    'f3000:FIGHT-PARSER-HIT';
1;