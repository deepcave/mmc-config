package mmc::fight::parsers::dead;

P::trig {
	my $cname = $1;
    var::up("mmc","lastMob",$cname);    
    mmc::em::put("dead",$cname);
    mmc::em::put("new_corp",$cname);
} 
    '^(.*) �����\S*, �\S+ ���� �������� ���������� � ������\.',
    'f100:FIGHT-LOOK';

P::trig {
    var::up("mmc","lastMob",$1);
    mmc::em::put("dead",$1);
}
    '^(.*) ��������\S* � ��������\S+ � ����\.$',
    'f9:FIGHT-LOOK';

P::trig {
    $mmc::fight::sx=0;
    var::up("mmc","lastExp",$1);    
    mmc::em::put("mob_gain_exp",$1);
} '^��� ���� ��������� �� (\d+) ���.','f1200:FIGHT-LOOK';

P::trig {
    $mmc::fight::sx=0;
    var::up("mmc","lastExp",1);    
    mmc::em::put("mob_gain_exp",1);
} '^���� ������ ����� �� ������.$','f1200:FIGHT-LOOK';

P::trig {
    $mmc::fight::sx=0;
    var::up("mmc","lastExp","0");
    mmc::em::put("mob_gain_exp",1);
} '^��� ���� ��������� ����� ���� �� ��������� ��������.','f1200:FIGHT-LOOK';

1;