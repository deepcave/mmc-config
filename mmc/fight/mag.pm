package mmc::fight::mag;
use M::utils;

sub onMirror {
    $fight::mag::mirror = 1;
}

sub doReset {#�������� ��� ���� ��� ���� ��� ����� �� �������
    my $opt = shift;
    $mmc::fight::mag::cast = 0;
    if (var::get('spell','����') eq "��") {
        P::sendl('~');
        P::echo "\003J��� ���� \003K �������";
    }
}

sub onCast {
    $mmc::fight::mag::cast = 0;
    autoCast;
}

sub onPrepare {
    my ($spell,$target) = @_;
    $mmc::fight::mag::cast = 1;
}

sub autoCast {#���������� ������ ��� ��� ���-�� �������
    return if (!$mmc::char::fight); #��� ��� �� ��������
    return if ($mmc::fight::mag::cast); #���� ��� ���-�� ������ - �� ��������
    my $frag = var::get('spell','���');
    return if (!$frag||$frag eq "���");
    P::sendl sprintf "� !%s!",$frag;
    $mmc::fight::mag::cast = 1;
}

sub do {#����� �� ���� (������)
    my ($target) = @_;
    my ($first) = var::get('spell','����');
    return if (!$first || $first eq "���");
    my $string = sprintf "� !%s! %s",$first,$target;
    P::sendl($string);
}

sub bpMessage {#��������
    my ($spell,$target)=@_;
    my $string = sprintf
        "[%s]\003K##���:%s\003P# ���� \003K%s",
            M::strTime(),
            ($mmc::char::fight?"\003J���":"\003B ��"),
            $spell,
            $target?$target:"���"
    ;
    P::wecho 6,$string;
}

BEGIN {
    $mmc::fight::mag::cast = 0;
    $mmc::fight::mag::order = 0;
    mmc::em::reg("prepare_cast",\&onPrepare);
    mmc::em::reg("cast",\&onCast);
    mmc::em::reg("end_fight",\&doReset);
}

1;