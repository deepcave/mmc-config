package mmc::wld::cmd::path;
use mmc::utils::var::cmd;

mmc::var::cmd::add(".����","path","���������");
mmc::var::cmd::setcmd("path",'���','cmd_path_step','���� ������');
mmc::var::cmd::setcmd("path",'����','cmd_path_stop','��������� ���������');
mmc::var::cmd::setcmd("path",'����','cmd_path_zone','���� � ����');
mmc::var::cmd::setcmd("path",'�����','cmd_path_points','���� � ����');
mmc::var::cmd::set("path",'������','\d{1,3}','������������ ������ ��������');
mmc::var::cmd::setcmd("path",'default','cmd_path','������');
mmc::var::cmd::setcmd("path",'����','cmd_map_zones','������ ���');



sub cmd_path_points {
 
}

sub cmd_path_zone {
    return showZoneExits(@_);
}

sub showExits ($) {
    my $roomNum = $_[0];
    my $room = wld::rooms($roomNum);
    if (!$roomNum) {
        CL::warn("�� ������� ������� � ����");
        return;
    }
    if (!$room) {
        CL::warn("�� ������� ������� � ����");
        return;
    }
    return showZoneExits($room->{zoneNum});
}

sub showZoneExits {
    my ($zoneNum) = @_;
    my $zone = wld::zones($zoneNum);
    if (!$zone) {
        CL::warn("�� ������� ���� ����������");
        return;
    }
    my $i = 0;
    P::echo("\003P ___________________________________________________________________________________ ");
    P::echo(
        sprintf "\003P|           \003K% -70s\003P  |",$zone->{name}
        );
    P::echo("\003P|-----------------------------------------------------------------------------------|");
    P::echo("\003P|    |        |                                  |                                  |");
    my @res = ();
    while (my ($eRoomNum,$eRooms) =each(%{$zone->{exits}})) {
        $i++;
        if ($i>1) {
            P::echo("\003P|----|--------|----------------------------------|----------------------------------|");
        }
        push (@res,sprintf('%s:%s',$i,$eRoomNum));
        my $mRoomName = wld::rooms($eRoomNum)->{name};
        my $bt = "K";
        $bt = "H" if ($eRoomNum == var::get("map","room"));

        P::echo sprintf ("\003P| \003$bt% 2u \003P| % 6u | \003K% -32s \003P|\003O % -32s \003P|",$i,$eRoomNum,
            substr($mRoomName,0,32),
            substr($zone->{name},0,32)
            
        );
        while (my ($eRoomNum,$eZone) = each(%{$eRooms})) {
            P::echo sprintf ("\003P| \003K% 2s \003P| \003B% 6u \003P| \003K% -32s \003P|\003O % -32s \003P|",
                "",$eRoomNum,
                substr($eZone->{name},0,32),
                substr(wld::rooms($eRoomNum)->{name},0,32)
            );
        }
    }
    P::echo("\003P|____|________|__________________________________|__________________________________|");
    var::up("map","cmd_buff",join(" ",@res));
}

sub goToRoomNum {
    my $roomNum = shift(@_);
    @route = wld::route(var::get("map","room"),$roomNum);
    if (!@route) {
        CL::warn "������ �� �������";
        return;
    }
    P::echo "����:@route " if (mmc::mode::debug("route"));
    P::echo "��������: ".wld::walk(@route);
    return wld::walk::go(1);    
}

sub goToBuffPoint {
    my $n = shift(@_);
    my $ok = 0;
    for my $t (split(/\s+/,var::get("map","cmd_buff"))) {
        if ($t=~/(\d+):(\d+)/) {
            $ok = $2 if ($1==$n);
        } 
    }
    if (!$ok) {
        CL::warn("��� ����� ����� � ������");
    } else {
        return goToRoomNum($ok);        
    }
}

sub cmd_path {    
    return showExits(var::get("map","room")) if (!@_);
    my $n = shift(@_);
    P::msg "==$n:@_";
    if ($n < 100) {
        return goToBuffPoint($n);
    } elsif ($n < 3000) {
        #����������� �����
    } elsif ($n < 300000) {
        return goToRoomNum($n);
    } else {
        P::msg "�� ���� ���� ����?"
    }
}

sub cmd_path_step {
    return wld::walk::go(1);
}

mmc::em::reg("cmd_path_step",\&cmd_path_step);
mmc::em::reg("cmd_path",\&cmd_path);
mmc::em::reg("cmd_path_zone",\&cmd_path_zone);
1;