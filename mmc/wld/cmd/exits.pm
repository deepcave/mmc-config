package mmc::wld::cmd::exits;
use mmc::utils::var::cmd;
mmc::var::cmd::add(".�����","mapper","���������");
mmc::var::cmd::set("mapper",'������','��|���','���������� ������');
mmc::var::cmd::set("mapper",'��','��|���','���������� ����� �� ��������� ���������');

sub showExits {

    my $room = var::get("map","room");
    my $exits = wld::exits($room);
    my $moves = wld::moves($room);

    while (my ($ed,$eRoomNum) = each (%$exits)) {
        my $eRoom = wld::rooms($eRoomNum);
        if (!$eRoom) {
            P::echo sprintf(
                "\003G###�����\003J����������� ������� �� \003K%s \003J%s",
                    wld::directs::name($ed,2),
                    $eRoomNum
            );
            P::sendl(
                sprintf("�� ������� �� %s � ��������.",wld::directs::name($ed,1))
            );
        }
        if ($moves->{$ed}) {
            P::echo sprintf(
                "\003G###�����\003K ����� �� \003K%s \003J%s \003H[\003O%s\003H]",
                    wld::directs::name($ed,2),
                    $eRoom->{name},
                    $eRoomNum
            );
        } else {
            P::echo sprintf(
                "\003G###�����\003J ����� �� \003K%s \003J%s \003H[\003O%s\003H] $moves",
                    wld::directs::name($ed,2),
                    $eRoom->{name},
                    $eRoomNum
            );
        }
    }
}

mmc::em::reg("mapper_show_exit",\&showExits);
1;