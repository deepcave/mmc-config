package mmc::wld::cmd::map;
use mmc::utils::var::cmd;
mmc::var::cmd::add(".�����","mapper","���������");
mmc::var::cmd::set("mapper",'������','��|���','���������� ������');
mmc::var::cmd::set("mapper",'��','��|���','���������� ����� �� ��������� ���������');
mmc::var::cmd::setcmd("mapper",'����','cmd_map_zones','������ ���');
mmc::var::cmd::setcmd("mapper",'default','cmd_map','�������� �����');


sub cmd_show_zones {
    my $pat = join(' ',@_);
    my %zones = wld::zones::__getAll();
    P::echo "\003K ����� ��� ".($pat?"($pat)":"");
    while (my($num,$zone)=each(%zones)) {
        P::echo sprintf (" \003K% 4u | \003O%s",
            $num,$zone->{name}
        ) if ($zone->{name} =~ $pat);
    }
}

mmc::em::reg("cmd_map_zones",\&cmd_show_zones);

1;