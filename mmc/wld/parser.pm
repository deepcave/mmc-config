package mmc::wld;

P::trig {
    my ($from,$to) = (mmc::var::get("map","prevRoom"),mmc::var::get("map","room"));
    mmc::em::put("going",$2,$from,$to);
}
    '^�� ���������( ��)? (\S+)\.$',
    'f10:WLD-MOVE';


sub disableWLA {
    if (! $mmc::wld::larr) {
        return;
    }
    $mmc::wld::larr = 0;
    P::disable('WLD-LOOK-ARR');
    return;
}


P::trig {
    $mmc::wld::larr = 1;
    P::enable('WLD-LOOK-ARR');
}
    '^�� ���������� �� ��������.$',
    'f100:WLD-LOOK';


P::trig {

    if (var::get("mapper","������")) {
        mmc::em::put("mapper_show_exit");
    }
    return;
}
    '^\[ Exits\: (.*) \]$',
    'fg10:WLD';

P::trig {
    mmc::em::put("hour");
}
    '^����� ���.$',
    'f10:WLD-TIME';

mmc::em::reg("prompt",\&disableWLA);

1;