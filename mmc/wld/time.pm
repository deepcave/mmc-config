package mmc::wld::time;
use mmc::var;
use M::em::em;

sub setTick {
    var::up("mmc","tick",120);
}

sub processTimer {
    my @lt = localtime();
    if (var::get("mmc","tick")<0) {#for unreal timers
        var::up("mmc","tick",31);
    }
    my $hh = var::get("mmc","hour");
    $hh = 0 if (!$hh);
    my $h1 = sprintf ("%u",$lt[2]);
    my $h0 = sprintf ("%u",$hh);

    if (!($h0 eq $h1)) {
        mmc::em::put("real.hour");
    }
    
    var::up("mmc","hour",$lt[2]);
    var::up("mmc","tick",var::get("mmc","tick")-1);
    var::up("mmc","uptime",var::get("mmc","uptime")+1);
    var::up("mmc","time",time());
    var::up("mmc","clock",M::strTime);
    my $tick = var::get("mmc","tick");
    mmc::em::put("half_tick") if ($tick == 90 || $tick == 30);
    mmc::em::put("pre_tick") if ($tick == 2 || $tick == 62);
    return 1;
}


BEGIN {
    var::up("mmc","tick",120);
    var::up("mmc","uptime",120);
    mmc::em::reg("hour",\&setTick);
    mmc::em::reg("tick_second",\&processTimer);

    P::timeout {
            mmc::em::put("tick_second",0);
    } "1000";
}
1;