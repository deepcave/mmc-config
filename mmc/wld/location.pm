package mmc::wld;
use wld::wld;
use M::em::em;

sub setRoom {
    my $room = shift;
    my $exits = $room->{EXITS};
    return if ($room->{VNUM} == mmc::var::get("map","room"));

    mmc::var::up("map","terrain",$room->{TERRAIN});
    mmc::var::up("map","prevRoom",mmc::var::get("map","room"));
    mmc::var::up("map","prevZone",mmc::var::get("map","zone"));
    
    mmc::var::up("map","room",$room->{VNUM});
    mmc::var::up("map","zone",$room->{ZONE});
    
    mmc::var::up("map","roomName",$room->{NAME});
    mmc::var::up("map","zoneName",$room->{AREA});

    while (my ($direct,$eRoom)=each(%$exits)) {
        mmc::em::put("get_exit",$room->{VNUM},$eRoom,$direct);
    }
    
    if (! wld::zones($room->{ZONE},$room->{AREA})) {
        wld::zones::add($room->{ZONE},$room->{AREA});
    }

    if (! wld::rooms($room->{VNUM},$room->{NAME})) {
        wld::rooms::add($room->{VNUM},$room->{NAME},$room->{TERRAIN})
    }
}

sub onMove {
    my ($direct,$from,$to) = @_;
    my $sdirect = wld::direct($direct);
    wld::moves::save($from,$to,$direct);
    P::msg "have move from $from [$direct($sdirect)] to room $to" if (mmc::mode::debug("map"));
}

mmc::em::reg("going",\&onMove);
mmc::em::reg("msdp_room",\&setRoom);
1;