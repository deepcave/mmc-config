package mmc::wld::map::exits;

sub strExit ($) {
    my %se = (
        's'=>'�',
        'n'=>'�',
        'e'=>'�',
        'w'=>'�',
        'u'=>'^',
        'd'=>'v'
    );
    return $se{$_[0]}
}

sub showExit {
    my ($srcRoomNum,$direct) = @_;
    my $srcRoom = wld::rooms($srcRoomNum)
}


sub getExitType {
    my ($from,$direct) = @_;
    my $dir = wld::directs::shDirect($direct);
    my $exits = wld::exits($from);
    my $exitRoom = $exits->{$dir};
    if (!wld::rooms($exitRoom)) {
        #������� ��� � ����
        return 1;
    }
    my $moves = wld::moves($from);
    my $goed = 0;
    while (my ($gt,$gd)=each(%$moves)) {
        if ($gd eq $dir) {
            $goed = 1;
        }
    }
    return 2 if (!$goed);#������� �� ������ � ������� ������
    my $ERexit  = wld::exits($exitRoom);
    my $ERmoves = wld::moves($exitRoom);
    if (! keys(%$ERmoves)) {
        #������ �� ������� �� ������ �� �����
        return 3;
    }
    if (! keys(%$ERexit)) {
        #������ ��� �������
        return 4;
    }
    
    return 0;
}


sub showExits {

    my $room = var::get("map","room");
    my $exits = wld::exits($room);
    my $moves = wld::moves($room);
    my %hmoves = ();
    while(my($mRoomNum,$mDirect) = each(%$moves)) {
        $hmoves{$mDirect}{roomNum}=$mRoomNum;
        my $ec=0;
        my $tmp = wld::moves($mRoomNum);
        my @mt = ();
        while (my($oRoomNum,$oDirect) = each(%$tmp)) {
            $ec++;
            push (@mt,$oDirect);
            $hmoves{$mDirect}{exits}{$oDirect}=$oRoomNum;
        }
        @{$hmoves{$mDirect}{moves}}=@mt;

    }
    
    $wld::exits = "";
    while (my ($ed,$eRoomNum) = each (%$exits)) {
        my $strEd = strExit($ed);
        my $eRoom = wld::rooms($eRoomNum);
        if (!$eRoom) {
            P::echo sprintf(
                "\003G###�����\003L ����� �� \003J%s \003B ����������� ������� \003H[\003B%s\003H]",
                    wld::directs::name($ed,2),
                    $eRoomNum
            );
            $wld::exits.="\003J$strEd";
            P::sendl(
                sprintf("�� ������� �� %s � ��������.",wld::directs::name($ed,1))
            ) if (var::get("group","�������") eq "��");
            next;
        }
        if ($hmoves{$ed}{moves}) {
            P::echo sprintf(
                "\003G###�����\003L ����� �� \003K%s \003C%s \003H[\003O%s\003K] \003D �������: \003I%s",
                    wld::directs::name($ed,2),
                    $eRoom->{name},
                    $eRoomNum,
                    join (' ',@{$hmoves{$ed}{moves}}).'.'
            );
            $wld::exits.="\003K$strEd";
        } else {
            P::echo sprintf(
                "\003J###�����\003L ����� �� \003J%s \003C%s \003H[\003O%s\003H] \003D �������: %s",
                    wld::directs::name($ed,2),
                    $eRoom->{name},
                    $eRoomNum,
                    join(' ',@{$hmoves{$ed}{moves}}).'.'
            );
            $wld::exits.="\003D$strEd";
            P::sendl(
                sprintf("�� ������ �� ������� �� ������� �� %s, ����� ��� ��?",wld::directs::name($ed,1))
            ) if (var::get("group","�������") eq "��");
        }
    }
    P::echo "\n\n";
}

mmc::em::reg("mapper_show_exit",\&showExits);
1;