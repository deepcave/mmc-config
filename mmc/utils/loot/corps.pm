package mmc::utils::loot::corps;


sub loot_corps {
	if (var::get("mode","���") eq "���") {
        M::info("��� ��������");
        return;
    }
    if (var::get("loot","����") eq "���" && $mmc::char::fight) {
        M::info("��� ����� ���");
        return;
    }
    P::sendl("����� ���.����") if (var::get("loot","���������") eq "��");
    P::sendl("����� ��� ���.����") if (var::get("loot","�����") eq "��");
    #P::sendl("������� ����") if (var::get("loot","�������") eq "��");
}

sub loot_drops {
    my $item_name = shift;
    if (var::get("mode","���") eq "���") {
        M::info("��� ��������");
        return;
    }
    P::echo sprintf "\003L###\003P[\003D����\003P] \003K%s",$item_name;
    #mmc::utils::loot::log($item_name);
    P::sendl("����� ".M::shortMudName($item_name));
}

sub loot_corps_after_fight {
    return if (var::get("loot","����") eq "��" && $mmc::char::fight);    
}
mmc::em::put("loot_drop",\&loot_drops);
mmc::em::reg("new_corp",\&loot_corps);
mmc::em::reg("end_fight",\&loot_corps_after_fight);
1;