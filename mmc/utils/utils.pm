package mmc;
use mmc::utils::logs::auto;
use mmc::var;
use M::em::em;

sub connect {
    my $server  = var::get('main','server');
    my $port    = var::get('main','port');

    if ($server && $port) {
        CMD::cmd_connect($server,$port);
    }
}

sub quit {
    CMD::cmd_quit();
}
sub reconect {
    if ($::hoststatus eq '*not connected*') {
        P::msg "reconect" ;
        if (var::get("mode","������������") eq "��") {
           mmc::connect();
        }
    }
}

mmc::em::reg("pre_tick",\&reconect);

1;