package mmc::msdp::group;
use Data::Dump qw(dump);

sub get {
    my $group = shift;
    mmc::em::put("fightRound");
    for my $member (@$group) {
        mmc::em::put (
            "setGroupMember",
            $member->{NAME},
            $member->{POSITION},
            $member->{HEALTH},
            $member->{MOVE},
            $member->{ROLE},
            $member->{IS_HERE},
            $member->{AFFECTS}
        );
    }
}
1;