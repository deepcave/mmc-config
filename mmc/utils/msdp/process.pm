package mmc::msdp;
use Data::Dump qw(dump);
use mmc::utils::msdp::room;
use mmc::utils::msdp::group;


sub decodeArray {
    my $line = shift;
    my $len = 0;
    my $last = 0;
    my $close = 0;
    my @result = ();
    for $sh (unpack('C*',$line)) {
        $len ++;
        $close++ if (chr($sh) eq '{' || chr($sh) eq '[');
        $close-- if (chr($sh) eq '}' || chr($sh) eq ']');
        if (chr($sh) eq ':' && $close == 0) {
            if ($last && $len) {
                my $sub = substr($line,$last+1,$len-$last-2);
                my $item = decodeMap($sub);
                push(@result,$item);
            }
            $last = $len;
        }
    }
    if ($last && $len) {
        my $sub = substr($line,$last+1,$len-$last-2);
        my $item = decodeMap($sub);
        push(@result,$item);
    }
    return \@result;
}

sub decodeMap {
    my $line = $_[0];
    my %result = ();
    my $end = 0;
    $it = 100;
    while ((!$end)&&$it--) {
        if ($line =~ /^#([^:]+):(.*)$/) {
            my $var = $1;
            my $sub = $2;
            my $val = "";
            $line =~ s/^#([^:]+)://g;
            if ($sub =~ /^\{/) {
                my $len = 0;
                my $close = 0;
                for $sh (unpack('C*',$sub)) {
                    $len ++;
                    $close++ if (chr($sh) eq '{');
                    $close-- if (chr($sh) eq '}');
                    last if (!$close);
                }
                my $array = substr($sub,1,$len-2);
                #P::msg "MAP $var:$array";
                $result{$var} = mmc::msdp::decodeMap($array);
                $line = substr($sub,$len);
            } elsif ($sub =~ /^\[/) {
                my $len = 0;
                my $close = 1;
                for $sh (unpack('C*',$sub)) {
                    $len ++;
                    $close++ if (chr($sh) eq '[');
                    $close-- if (chr($sh) eq ']');
                    last if (!$close);
                }
                my $array = substr($sub,1,$len-2);
                $result{$var} = mmc::msdp::decodeArray($array);
            } else {
                $sub =~s/^([^#]*)//;
                $val = $1;
                $line =~s/^[^#]*//g;
                $result{$var}=$val;
            }
        } else {
            $end = 1;
        }
    }
    #P::echo dump(%result);
    return \%result;
}

sub decode {
    my $line = $_[0];
    if ($line =~ /^#([^:]+):(.*)$/) {
        P::msg("get var $1 => $2");
        my %result = ();
        $result{$1}=mmc::msdp::decode($2);
        return $result;
    } elsif ($line =~ /^\{(.*)\}$/) {
        return mmc::msdp::decodeMap($1);
    } elsif ($line =~ /^\[(.*)\]$/) {
        P::msg("get array");
        #return mmc::msdp::decodeArray($1);
    } else {
        P::msg("get text");
        return $line;
    }
}

sub handler ($) {
    my $line = $_[0];
    my $message = mmc::msdp::decodeMap($line);

    if ($message->{ROOM}) {
        mmc::msdp::room::get($message->{ROOM});
    } elsif ($message->{GROUP}) {
        mmc::msdp::group::get($message->{GROUP});
    } else {

    }
    P::wecho(8,"\003P#MSDP#\003H".$line);
}

sub subscribe {
    MUD::msdp("REPORT","ROOM");
    MUD::msdp("REPORT","GROUP");
}

P::trig {
        mmc::msdp::handler($1);
}
    '^###MSDP###(.*)$',
    'g1:MSDP';
1;