package mmc::var::cmd;

sub __table {
    my ($mode,$pkey) = @_;
    if (!$mode) {
        CL::warn("!������!, �� ��������� ����� ��� �������");
        return;
    }
    
    my $maxLen = 11;
    while (my($key,$help)=each(%{$data{$mode}{helps}})) {
        my $clen = length(mmc::var::get($mode,$key));
        $maxLen=$clen if ($clen>$maxLen) ;
    }

    my $topLine = '_______________________________________________________________________________________________________';
    
    my $lineFormat = "| \003P% 16s \003H| %s% -".$maxLen."s \003H| \003D%s";

    my $botFormat = "|% 18s|% ".($maxLen+2)."s|";
    P::echo "\003K  ".$data{$mode}{descr}.":";
    P::echo " ".substr($topLine,0,$maxLen+16+3+2+2-2)." ";
    
    P::echo sprintf $lineFormat;
    while (my($key,$help)=each(%{$data{$mode}{helps}})) {
        if (!$_[1]||$key eq $_[1]) {
            P::echo sprintf $lineFormat,
                $key,
                mmc::var::get($mode,$key) eq '���'?"\003B":"\003K",
                mmc::var::get($mode,$key),
                $help;
        }
    }
    P::echo sprintf $botFormat,substr($topLine,0,18),substr($topLine,0,$maxLen+2);
    P::echo "";
}

sub __help {
    my ($mode,$pkey) = @_;
    if (!$mode) {
        CL::warn("!������!, �� ��������� ����� ��� �������");
        return;
    }
    
    my $maxLen = 11;
    while (my($key,$help)=each(%{$data{$mode}{helps}})) {
        my $clen = length(mmc::var::get($mode,$key));
        $maxLen=$clen if ($clen>$maxLen) ;
    }

    my $topLine = '_______________________________________________________________________________________________________';
    my $lineFormat = "| \003P% 16s \003H| %s% -".$maxLen."s \003H| \003D%s";
    my $botFormat = "|% 18s|% ".($maxLen+2)."s|";
    P::echo "\003K  ".$data{$mode}{descr}.":";
    P::echo " ".substr($topLine,0,$maxLen+16+3+2+2-2)." ";

    P::echo sprintf $lineFormat;
    while (my($key,$help)=each(%{$data{$mode}{helps}})) {
        if (!$_[1]||$key eq $_[1]) {
            P::echo sprintf $lineFormat,
                $key,
                mmc::var::get($mode,$key) eq '���'?"\003B":"\003K",
                mmc::var::get($mode,$key),
                $help;
        }
    }
    P::echo sprintf $botFormat,substr($topLine,0,18),substr($topLine,0,$maxLen+2);
    P::echo "";
}

sub setcmd {
    my ($mode,$key,$event,$help)= @_;
    if (!($mode&&$key)) {
        CL::warn("������ ���������� �������, ���-�� �� ����������");
        return;
    }
    if ($key eq "default") {
        $data{$mode}{default_event}=$event;
        return;
    }
    if (!$data{$mode}) {
        CL::warn("������ ���������� ��������, �� ����� ��� �����");
        return;
    }
    $pattern = ".*" if (!$pattern);
    $help = "��� ��������" if (!$help);
    $data{$mode}{events}{$key}=$event;
    $data{$mode}{helps}{$key}=$help;    
}

sub set {
    my ($mode,$key,$pattern,$help)= @_;
    if (!($mode&&$key)) {
        CL::warn("������ ���������� ������, ���-�� �� ����������");
        return;
    }
    if (!$data{$mode}) {
        CL::warn("������ ���������� ������, �� ����� ��� �����");
        return;
    }
    $pattern = ".*" if (!$pattern);
    $help = "��� ��������" if (!$help);
    $data{$mode}{modes}{$key}=$pattern;
    $data{$mode}{helps}{$key}=$help;
    
}

sub add {
    my ($cmd,$mode,$descr) = @_;
    if (!$descr) {
        CL::warn("������ �������� ������� ���������, �� �������� ���������");
        return;
    }
    if ($data{$mode}) {
        CL::warn("����� $mode ��� ���������");
        return;
    }
    $data{$mode}=();
    $data{$mode}{cmd}=$cmd;
    $data{$mode}{descr}=$descr;
    P::alias {
        my $key = $_[0];
        if ($key eq "?") {
            __help($mode,$val);
            return;
        }
        if ($data{$mode}{events}{$key}) {
            shift(@_);
            return mmc::em::put($data{$mode}{events}{$key},@_);
        }
        if (!$key) {
            return __table($mode) if (!$data{$mode}{default_event});
            return mmc::em::put($data{$mode}{default_event});
        }
        if (!$data{$mode}{modes}{$key}) {
            return __table($mode) if ($key eq "���");
            if ($data{$mode}{default_event}) {
                return mmc::em::put($data{$mode}{default_event},@_);
            }
            CL::warn(sprintf("%s: �������������� �������� \"%s\"",$descr,$key));
            return;
        }
        shift(@_);
        $val = join(' ',@_);
        if ($val eq '') {
            __table($mode,$key);
            return;
        }
        my $pattern = '^'.$data{$mode}{modes}{$key}.'$';
        if ($val =~ /$pattern/) {
            mmc::var::set($mode,$key,$val);
            __table($mode,$key);
            return;
        } else {
            CL::warn(sprintf ("%s: �������� ������ ��������� \"%s\" (%s)",$mode,$key,$val));
            return;
        }
    } "$cmd";
}

BEGIN {
    %data = ();
}
1;