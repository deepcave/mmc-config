package mmc::prompt;
use M::utils;
use mmc::utils::msdp;
use mmc::char::char;
use mmc::fight::fight;



sub parse {
    my $line = shift;
    
    if ($line =~ /\[([^]]*)\]\s*\[([^]]*)\]\s*\[([^]]*)\]\s+\>\s+/g) {
        my ($me,$meState) = split(/\:/,$1);
		my ($tank,$tankState) = split(/\:/,$2);
		my ($target,$targetState) = split(/\:/,$3);
        mmc::char::setFight(2); #���� ���� �� ������
        mmc::fight::setTank($tank,$tankState);
        mmc::fight::setTarget($target,$targetState);
        mmc::fight::mag::autoCast();
        
	} elsif ($line =~ /\[([^]]*)\]\s*\[([^]]*)\]\s+>\s+$/g) {
        my ($me,$meState) = split(/\:/,$1);
		my ($target,$targetState) = split(/\:/,$2);
        mmc::char::setFight(2); #���� ���� �� ������
        mmc::fight::setTank($tank,$tankState);
        mmc::fight::setTarget($target,$targetState);
        mmc::fight::mag::autoCast();
        
	} else {#�������� ��� ����������
        mmc::char::setFight(0); #���� ���� �� ������
        mmc::fight::setTank();
        mmc::fight::setTarget();
        mmc::fight::round::stop(0);
        mmc::fight::mag::autoCast();
	}	 
    return $line;
}

sub modify {
    my $line = $_[0];
    my $delta = 0;
    $delta = mmc::char::setNLE($1) if ($line =~ /(\d+)�/);
    mmc::fight::round::onRound($delta) if ($delta>0);
    if ($line =~ /(\d+)H/) {
        mmc::char::setHp($1);
        if (!var::get("map","room")) {
            mmc::msdp::send("SEND","ROOM");
        }
    }
    mmc::char::setMv($1) if ($line =~ /(\d+)M/);
    $line =~ s/(\d+)�/&M::numToStr($1)/e;
    $line = mmc::char::flags::str()."\003G#".$line;
    if ($line =~ /���\:([����\^v]+)/) {
        my $exits = $1;
        my $cexits = "\003O���\003P:";
        for (my $i=0;$i<6;$i++) {
            my $E = substr($exits,$i,1);
            last if (!$E);
            my $etype = mmc::wld::map::exits::getExitType(var::get("map","room"),$E);
            my $color = $etype==0?"\003C":
                        $etype==1?"\003B":
                        $etype==2?"\003D":
                        $etype==3?"\003F":
                        "\003G";
            $cexits.="$color$E";
        }
        $cexits.="\003P";
        $line =~ s/���\:([����\^v]+)/$cexits/g;
    }
    return $line;
}


P::hook {
	    my $line = shift;
        my $cline = $line;
        $line =~ s/\003.//ge;
        mmc::em::reg("prompt");
        if ($line =~ /^Select one :\s+$/) {
            P::sendl "2";
            mmc::msdp::subscribe();
            return $cline;
        } elsif ($line =~ /������� ��� ��������� \(��� "�����" ��� �������� ������\):/) {
            mmc::char::login;
            mmc::log::dolog;
            return $cline;
        } elsif ($line eq '* � ����� � ���������� �������� ����� ANYKEY ������� ENTER *') {
            P::sendl " ";
            return $cline;
        } elsif ($line =~/^\s+���� ���� ���� ������/) {
            P::sendl "1" if (var::get("��������") =~ /��/);
        } else {
            mmc::prompt::parse($line);
        }
        return mmc::prompt::modify($cline);

} "prompt";

1;