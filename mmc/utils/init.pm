package mmc::utils;


BEGIN {
    P::msg("create dirs ");
    mkdir 'data' if (! -d 'data');
    mkdir 'data/logs' if (! -d 'data/logs');
    mkdir 'data/db' if (! -d 'data/db');
    mkdir 'data/tmp' if (! -d 'data/tmp');
}

1;