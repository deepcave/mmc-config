package mmc::utils::logs::auto;

sub mdir ($) {
    mkdir ($_[0]) if ( ! -d $_[0] );
}

sub dolog {
    my $dir = 'data/logs';
    my @date = localtime();
    mdir ($dir);
    $dir = sprintf('%s/%s',$dir,var::get('char','name'));
    mdir ($dir);
    $dir = sprintf('%s/%s-%02u',$dir,$date[5]+1900,$date[4]+1);
    mdir ($dir);    
    $dir = sprintf('%s/%02u',$dir,$date[3]);
    mdir ($dir);
    my $file = sprintf('%s/%u-%02u-%02u.%02u-%02u.log',$dir,$date[5]+1900,$date[4]+1,$date[3],$date[2],$date[1]);
    P::echo "open log $file";
    CMD::cmd_log($file);
}

mmc::em::reg("connect",\&dolog);
mmc::em::reg("real.hour",\&dolog);
1;