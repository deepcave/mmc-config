package mmc::utils::logs::dead;

sub db { return mmc::data::mud::db };

sub __saveToDb {
    db()->exe('INSERT INTO expLog (charName,mobName,zoneNum,roomNum,exp) VALUES (?,?,?,?,?)',@_);
}
sub onDead {
    my $name = shift;   
}

sub onMobGainExp {
    my $exp  = shift;

    my $name = var::get("mmc","lastMob");
    my $room = var::get("map","room");
    my $zone = var::get("map","zone");
    __saveToDb(var::get("char","name"),$name,$zone,$room,$exp);
    my $strname = $name;
    $strname =~ s/^(.{1,32}).*$/$1/;
    M::log::misk "\003F����", "\003L% -8u \003H| \003O% -32s \003H| \003I% -6u | \003N%s",
        $exp,
        $strname,
        $room,
        wld::rooms($room)->{name};
}

mmc::em::reg("mob_gain_exp",\&onMobGainExp,0);
mmc::em::reg("dead",\&onDead,0);
1;