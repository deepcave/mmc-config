package mmc::utils::logs;
use mmc::utils::logs::dead;

$log_window = 4;

sub line {
    my $line = shift;
    my $time = M::strTime();
    my $format = sprintf (sprintf "\003P[\003F%s\003P]:%s",$time,$line);
    P::wecho $log_window,sprintf($format,@_);
    return ;
}

1;