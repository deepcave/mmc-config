package mmc::utils::status;
use mmc::var;

sub add {
    my ($line,$mod,$name,$descr,$len,$color) = @_;

    my $cv = var::get($mod,$name);

    if (! defined $cv) {
        var::up($mod,$name,"");
    }
    P::msg sprintf("ADD STATUS VAR[%s][%s]:%s",$mod,$name,$mmc::var::data{$mod}{$name});
    push(@mmc::utils::status::ob,$ob="[");
    push(@mmc::utils::status::cb,$cb="]");
    $line = $line?$line-1:0;
    $len = $len?$len:0;
    $color = $color?$color:11;
    $descr{++$descrC} = $descr?sprintf " %s:",$descr:sprintf " ";
    Status::new_svy($descr{$descrC},$line,0,15) if ($descr);
    Status::new_svy($mmc::utils::status::ob[$#mmc::utils::status::ob],$line,0,14) if ($descr);
    Status::new_svy($mmc::var::data{$mod}{$name},$line,$len,$color) if ($descr);
    Status::new_svy($mmc::utils::status::cb[$#mmc::utils::status::cb],$line,0,14) if ($descr);
}

BEGIN {
    %descr = ();
    $descrC = "0";
    @mmc::utils::status::cb = ();
    @mmc::utils::status::ob = ();

    mkdir ('data/config') if (! -d 'data/config');
    if (!-f 'data/config/status.conf') {
        open(FC,'>data/config/status.conf');
        close(FC);
    }
    
    open(FC,'data/config/status.conf');
    my $line = "";
    while ($line = <FC>) {
        if ($line =~ /^(\S+)\s*=\s*(.*)$/) {
            my ($line,$len,$descr,$color) = split(/\s*,\s*/,$2);
            my ($mod,$name) = split(/:/,$1);
            if (!$name) {
                $name = $mod;
                $mod = 'mmc';
            }
            add($line,$mod,$name,$descr,$len,$color);
        }
    }
    close(FC);
}
1;