package mmc;
use mmc::utils::utils;

P::bindkey {
    mmc::connect();
} 'f12';

P::bindkey {
    mmc::quit();
} 'M-f12';

1;